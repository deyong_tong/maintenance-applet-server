<?php
session_start();
if(isset($_SESSION['admin'])){
	
?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>故障</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body class="">
      <div class="x-nav">

      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="iconfont" style="line-height:30px">&#xe6aa;</i></a>
    </div>
<div id="test9" class="demo-tree demo-tree-box" style="background-color:#fff;width: 200px; height: 300px; overflow: scroll;"></div>

<script>
layui.use(['tree', 'util'], function(){
  var tree = layui.tree
  ,layer = layui.layer
  ,util = layui.util
  
  ,data1 = [{
    title: '江西'
    ,id: 1
    ,children: [{
      title: '南昌'
      ,id: 1000
      ,children: [{
        title: '青山湖区'
        ,id: 10001
      },{
        title: '高新区'
        ,id: 10002
      }]
    },{
      title: '九江'
      ,id: 1001
    },{
      title: '赣州'
      ,id: 1002
    }]
  },{
    title: '广西'
    ,id: 2
    ,children: [{
      title: '南宁'
      ,id: 2000
    },{
      title: '桂林'
      ,id: 2001
    }]
  },{
    title: '陕西'
    ,id: 3
    ,children: [{
      title: '西安'
      ,id: 3000
    },{
      title: '延安'
      ,id: 3001
    }]
  }];
  
  tree.render({
    elem: '#test9'
    ,data: data1
    ,edit: ['add', 'update', 'del'] //操作节点的图标
    ,click: function(obj){
      layer.msg(JSON.stringify(obj.data));
    }
  });

});
</script>
</body>
</html>
<?php
}else{
	echo '<script>window.parent.location.href="login.php";</script>';
}
?>