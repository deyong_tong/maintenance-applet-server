<?php
	session_start();
	if(!isset($_SESSION['admin'])){
		header("location:login.php");
		die();
	}
?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>报修管理系统</title>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <link rel="stylesheet" href="./css/theme1178.min.css">
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            // 是否开启刷新记忆tab功能
            // var is_remember = false;
        </script>
    </head>
    <body class="index">
	
        <!-- 顶部开始 -->
        <div class="container">
            <div class="logo">
                <a href="/">报修系统</a></div>
            <div class="left_open">
                <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
            </div>
            <ul class="layui-nav right" lay-filter="">
                <li class="layui-nav-item">
                    <a href="javascript:;"><?php echo $_SESSION['ming']; ?></a>
                    <dl class="layui-nav-child">
                        <!-- 二级菜单 -->
                        <dd>
                            <a onclick="xadmin.open('修改密码','changepwd.php?user=<?php echo $_SESSION['admin']; ?>',500,300)">
                                修改密码</a></dd>
                        <dd>
                            <a href="./action.php?mode=logout">退出</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
        <!-- 顶部结束 -->
        <!-- 中部开始 -->
        <!-- 左侧菜单开始 -->
        <div class="left-nav">
            <div id="side-nav">
                <ul id="nav">
					<?php if($_SESSION['juese']==3){ ?>
					<li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="维修记录">&#xe6fa;</i>
                            <cite>维修记录</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
							<ul class="sub-menu">
								<li>
									<a onclick="xadmin.add_tab('维修记录','weixiu.php')">
										<i class="layui-icon layui-icon-util" style="font-size: 18px;"></i>   
										<cite>维修记录</cite></a>
								</li>
								<li>
									<a onclick="xadmin.add_tab('导出历史','daochu.php')">
										<i class="iconfont">&#xe714;</i>
										<cite>导出历史</cite></a>
								</li>
							</ul>
					</li>
					<?php } ?>
					<?php if($_SESSION['juese']==1){ ?>
					<li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="报修记录">&#xe6b5;</i>
                            <cite>报修记录</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
							<ul class="sub-menu">
								<li>
									<a onclick="xadmin.add_tab('报修记录','baoxiu.php')">
										<i class="iconfont">&#xe6a7;</i>
										<cite>报修记录</cite></a>
								</li>
							</ul>
					</li>
					<?php } ?>
					<?php if($_SESSION['juese']==2){ ?>
					<li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="订单记录">&#xe74e;</i>
                            <cite>订单记录</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
							<ul class="sub-menu">
								<li>
									<a onclick="xadmin.add_tab('订单记录','dingdan.php')">
										<i class="iconfont">&#xe6a7;</i>
										<cite>订单记录</cite></a>
								</li>
							</ul>
					</li>
					<li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="系统管理">&#xe6ae;</i>
                            <cite>系统管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
							<ul class="sub-menu">
								<li>
									<a onclick="xadmin.add_tab('故障类型','/admin/treegrid/index.php')">
										<i class="iconfont">&#xe6a7;</i>
										<cite>故障类型</cite></a>
								</li>
								<li>
									<a onclick="xadmin.add_tab('单位管理','danwei.php')">
										<i class="iconfont">&#xe6a7;</i>
										<cite>单位管理</cite></a>
								</li>
								<li>
									<a onclick="xadmin.add_tab('人员管理','renyuan.php')">
										<i class="iconfont">&#xe6a7;</i>
										<cite>人员管理</cite></a>
								</li>
							</ul>
					</li>
					<?php } ?>
					
                </ul>
            </div>
        </div>
        <!-- <div class="x-slide_left"></div> -->
        <!-- 左侧菜单结束 -->
        <!-- 右侧主体开始 -->
        <div class="page-content">
            <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
                <ul class="layui-tab-title">
                    <li class="home">
                        <i class="layui-icon">&#xe68e;</i>我的桌面</li></ul>
                <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
                    <dl>
                        <dd data-type="this">关闭当前</dd>
                        <dd data-type="other">关闭其它</dd>
                        <dd data-type="all">关闭全部</dd></dl>
                </div>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe src='./welcome.php' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
                    </div>
                </div>
                <div id="tab_show"></div>
            </div>
        </div>
        <div class="page-content-bg"></div>
        <style id="theme_style"></style>
        <!-- 右侧主体结束 -->
        <!-- 中部结束 -->
	
    </body>

</html>