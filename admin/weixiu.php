<?php
session_start();
if(isset($_SESSION['admin'])&&$_SESSION['juese']==3){
	if(isset($_GET['wxsj'])){
		$wxsj=$_GET['wxsj'];
	}else{
		$wxsj='';
	}
?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>工程师-维修列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js?2.5.5" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  
  <body class="">
      <div class="x-nav">

      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="iconfont" style="line-height:30px">&#xe6aa;</i></a>
    </div>
   <div class="layui-card-body ">
	<form class="layui-form layui-col-space5">
		<div class="layui-input-inline layui-show-xs-block">
		  <div class="layui-input-inline">
			<input type="text" class="layui-input" id="wxsj" name="wxsj" placeholder="维修时间" value="<?php echo $wxsj; ?>">
		  </div>
		</div>
		<div class="layui-input-inline layui-show-xs-block">
			<button class="layui-btn" lay-submit="" lay-filter="sreach">
				<i class="layui-icon">&#xe615;</i></button>
		</div>
	</form>
  </div>
  <div class="x-body">
	<table class="layui-hide" id="test" lay-filter="test"></table>
  </div>
<script type="text/html" id="qianming">
	{{# if(d.bxzqm!=''){}}
		<img src="/mini/qianming/{{d.bxzqm}}">
	{{#}}}
</script>
<script type="text/html" id="qianming1">
	{{# if(d.gcsqm!=''){}}
		<img src="/mini/qianming/{{d.gcsqm}}">
	{{#}}}
</script>
<script type="text/html" id="qianming2">
	{{# if(d.xxzxqm!=''){}}
		<img src="/mini/qianming/{{d.xxzxqm}}">
	{{#}}}
</script>
<script type="text/html" id="toolbarDemo">
  <div class="layui-btn-container">
    <button class="layui-btn layui-btn-sm" lay-event="daochulist">导出</button>
  </div>
</script>
<script type="text/html" id="guzhangtu">
{{#
	var fn = function(){
		var img= d.img;
		if(img != '[]' && img !=''){
			console.log(img);
			img = img.replace('[', "");
			console.log(img);
			img = img.replace(']', "");
			console.log(img);
			var imgs = img.split(','); 
			console.log(imgs);
			img = '';
			for(var i=0;i<imgs.length;i++)
			{
				var a = imgs[i];
				a=a.replace(/'/g,'');
				a = '/mini/uploads/' + a;
				var b = 'onclick="xadmin.open(' + "'预览','" + a +"'" + ')"';
				img = img + '<img '+b+' width="60px" height="60px;" src="'+a+'" />';
			}
		}else{
			img = '';
		}
		return img;
	}
}}
{{ fn() }}
</script>
<script>
layui.config({
	base: '/admin/lib/layui-ext/soultable/',   // 模块目录
	version: 'v1.5.3'
}).extend({             // 模块别名
	soulTable: 'soulTable'
});
layui.use(['form','table','soulTable','laydate'], function(){
  var table = layui.table
  ,soulTable = layui.soulTable
  ,laydate = layui.laydate
  ,form = layui.form;
   laydate.render({
    elem: '#wxsj'
    ,range: true
  });
  table.render({
    elem: '#test'
    ,url:'./action.php?mode=getgcswentilist'
	,toolbar: '#toolbarDemo'
    ,defaultToolbar: ['filter', 'exports', 'print', {
      title: '提示'
      ,layEvent: 'LAYTABLE_TIPS'
      ,icon: 'layui-icon-download-circle'
    }]
    ,title: '用户数据表'
	,height: 'full-150'
	,where:{gcs:'<?php echo $_SESSION["admin"]; ?>',bxsj:'<?php echo $wxsj; ?>'}
	,rowDrag: {trigger: 'row', done: function(obj) {
		//拖拽行
		// 完成时（松开时）触发
		// 如果拖动前和拖动后无变化，则不会触发此方法
		console.log(obj.row) // 当前行数据
		console.log(obj.cache) // 改动后全表数据
		console.log(obj.oldIndex) // 原来的数据索引
		console.log(obj.newIndex) // 改动后数据索引
	}}
	,overflow: {
		type: 'tips'
		,hoverTime: 300 // 悬停时间，单位ms, 悬停 hoverTime 后才会显示，默认为 0
		,color: 'white' // 字体颜色
		,bgColor: 'blue' // 背景色
		,minWidth: 100 // 最小宽度
		,maxWidth: 500 // 最大宽度
	}
    ,cols: [[
      {type: 'checkbox', fixed: 'left'}
      ,{field:'id', title:'ID', width:80, sort: true, align: 'center'}
      ,{field:'xm', title:'项目', width:120, sort: true, filter: true}
	  ,{field:'gz', title:'故障', width:150, sort: true}
	  ,{field:'ms', title:'故障描述', width:100, sort: true}
	  ,{field:'img', title:'故障图(点击看大图)', width:300, sort: true, templet: '#guzhangtu'}
	  ,{field:'status', title:'状态', width:80, sort: true, filter: true}
	  ,{field:'bm', title:'部门', width:100, sort: true, filter: true}
	  ,{field:'lxr', title:'联系人', width:100, sort: true}
	  ,{field:'tel', title:'电话', width:120, sort: true}
	  ,{field:'dz', title:'地址', width:150, sort: true}
	  ,{field:'yysj', title:'预约时间', width:130, sort: true}
	  ,{field:'tbsj', title:'报修时间', width:150, sort: true}
	  ,{field:'bxr', title:'报修人', width:100, sort: true}
	  ,{field:'bxrdh', title:'报修电话', width:120, sort: true}
	  ,{field:'xxzx', title:'派单人', width:100, sort: true}
	  ,{field:'pdsj', title:'派单时间', width:150, sort: true}
	  ,{field:'xxzxyj', title:'派单意见', width:100, sort: true}
	  ,{field:'wxz', title:'工程师', width:100, sort: true}
	  ,{field:'gcsdh', title:'工程师电话', width:120, sort: true}
	  ,{field:'clyj', title:'处理意见', width:100, sort: true}
	  ,{field:'jsdj', title:'处理时间', width:150, sort: true}
	  ,{field:'wxxq', title:'维修详情', width:120, sort: true}
	  ,{field:'wxsj', title:'维修时间', width:150, sort: true}
	  ,{field:'zj', title:'总价', width:100, sort: true}
	  ,{field:'xxzxshz', title:'费用审核人', width:150, sort: true}
	  ,{field:'shyj', title:'审核意见', width:100, sort: true}
	  ,{field:'shsj', title:'审核时间', width:150, sort: true}
	  ,{field:'bxzqm', title:'被修者签名', width:100, sort: true, templet: '#qianming'}
	  ,{field:'gcsqm', title:'工程师签名', width:100, sort: true, templet: '#qianming1'}
	  ,{field:'xxzxqm', title:'费用审核者签名', width:100, sort: true, templet: '#qianming2'}
	  //,{field:'daochu', title:'导出', width:100, sort: true}
    ]]
    ,page: true
	,filter: {
		items:['column','data','editCondition','clearCache'] // 加入了清除缓存按钮     'condition','excel',
		,cache: true //增加缓存功能，（会导致单元格编辑失效）
		,bottom: true //隐藏底部
	}
	,done: function (res, curr, count) {
		soulTable.render(this);
		console.log(res);

		var state = "";
		for (var i in res.data) {
			var item = res.data[i];
			if (item.daochu == 1||item.zhuangtai!=8) {// 这里是判断需要禁用的条件（如：状态为0的）
				// checkbox 根据条件设置不可选中
				$('tr[data-index=' + i + '] input[type="checkbox"]').prop('disabled', true);
				state = "1";// 隐藏表头全选判断状态
				form.render();// 重新渲染一下
			}
		}

      if(state == "1"){
         // 根据条件移除全选 checkbox
         $('th[data-field=0] div').replaceWith('<div class="layui-table-cell laytable-cell-5-0-0"><span></span></div>');
       }else {
          //翻页显示全选按钮 checkbox
          $('th[data-field=0] div').replaceWith('<div class="layui-table-cell laytable-cell-1-0-0 laytable-cell-checkbox"><input type="checkbox" name="layTableCheckbox" lay-skin="primary" lay-filter="layTableAllChoose"><div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon layui-icon-ok"></i></div></div>');
       }

	}
  });



  //头工具栏事件
  table.on('toolbar(test)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      //自定义头工具栏右侧图标 - 提示
      case 'LAYTABLE_TIPS':
		location.href="./action.php?mode=downlosd";
		break;
	  case 'daochulist':
        var data = checkStatus.data;
        //layer.alert(JSON.stringify(data));
		if(data.length > 0){
			var s="";
			for(var i=0;i<data.length;i++){
				s = s + data[i].id + ",";
			}
			s = s.substr(s,s.length - 1);
			layer.alert(s);
		}else{
			layer.alert('无选中条目');
		}
		break;
    };
  });
});
</script>
</body>
</html>
<?php
}else{
	echo '<script>window.parent.location.href="login.php";</script>';
}
?>