<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>添加人员</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red">*</span>姓名
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="ming" name="ming" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red">*</span>电话
                      </label>
                      <div class="layui-input-inline">
                          <input type="number" id="tel" name="tel" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>
                      </div>
                  </div>
					<div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>选择单位</label>
                        <div class="layui-input-inline">
							<select name="xzdw" id="xzdw">
								<option value="0" selected="">选择单位</option>
								<?php 
									include_once('../config.php');
									$sql = "select id,name from danwei where status=1";
									$requ = mysqli_query($con,$sql);
									while($rs = mysqli_fetch_array($requ)){
										echo '<option value="'.$rs['id'].'">'.$rs['name'].'</option>';
									}
								?>
								
							</select>
						</div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red"> </span>
                      </div>
                    </div>
					<div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>选择角色</label>
                        <div class="layui-input-inline">
							<select name="xzjs" id="xzjs">
								<option value="0" selected="">选择角色</option>
								<option value="1" >报修者</option>
								<option value="2" >管理员</option>
								<option value="3" >工程师</option>
							</select>
						</div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">请正确选择角色</span>
                      </div>
                    </div>

                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" lay-filter="add" lay-submit="">
                          增加
                      </button>
                  </div>
              </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
				
			  //监听指定开关
			  form.on('switch(switchTest)', function(data){
				if(this.checked){
					$("#wdw").val(1);
				}else{
					$("#wdw").val(0);
				}
			  });


                form.on('submit(add)',function(data) {
                    console.log(data.field);
					var dwm=data.field.ming;
					var tel=data.field.tel;
					var xzdw=data.field.xzdw;
					var xzjs=data.field.xzjs;
					$.post("action.php",{mode:"addrenyuan",dwm:dwm,tel:tel,xzdw:xzdw,xzjs:xzjs},function(result){
						console.log(result);
						var r=JSON.parse(result);
						if(r.status!=0){
							layer.alert("添加成功", {icon: 6},function () {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
							});
						}else{
							layer.alert('增加失败',{icon: 5})
						}
					})
                    return false;
                });

            });</script>
        
    </body>

</html>
