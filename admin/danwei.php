<?php
session_start();
if(isset($_SESSION['admin'])&&$_SESSION['juese']==2){
	
?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>单位管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body class="">
      <div class="x-nav">

      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="iconfont" style="line-height:30px">&#xe6aa;</i></a>
    </div>
  <div class="x-body">
	<table class="layui-hide" id="test" lay-filter="test"></table>
  </div>
<script type="text/html" id="toolbarDemo">
  <div class="layui-btn-container">
    <button class="layui-btn layui-btn-sm" lay-event="deleteJiTang">删除</button>
	<button onclick="xadmin.open('添加','./add-danwei.php',480,300)" class="layui-btn layui-btn-sm" >添加</button>
  </div>
</script>
<script type="text/html" id="switchTpl">
  <input type="checkbox" name="zt" value="{{d.id}}" lay-skin="switch" lay-text="ON|OFF" lay-filter="Changezt" {{ d.status == 1 ? 'checked' : '' }}>
</script>
<script type="text/html" id="waibu">
{{# if(d.other==1){}}
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAQCAYAAAAbBi9cAAABkElEQVQ4T6WTP0hbURTGfye24NClIH0vbhULujh108VBxUXaTi462N3kxRYdikVcRPxz4+JUF6GFLB26ZRJU2i4OXYqbiCQvtiAFhwz2fSWPGMQ8Gv/c7XK/87vnfOcc4z7nvR54j9l5GDFkd+U8cepLwT5wEmat904gL69xE5+AcvsZXUcLVr01yHdaAmaBc/1lqDJj32pV3QrkOe0Z9MeBxqtyxj5fWnNjkO+khp/ibRjYylV/Y1A6rwlFPAoD20wy33f6BXTEb8Z2mLHJ6zpLOwWCtXqdG9Uq82dz9udS6Dv9BHrq9x8RDJ9mrdIMWtNzpSgAT2uPEkWM+UrWvvtOu8BAw4eIkXLOiklZx6X5eQ0SUcDq6cOx4MDgRSPIWA4zVutW4mmYnc7rpeAjoj1BeZi6YKD0xn63BNUE3rqmzPhwXSzxuhLY1v+2oKn96XXlZKxeafWXMLCxVquUOEee06LBu7iTKUbK08kGN81R028FtfklHNAdZm20VTb10UmWda6o46KNZ6eBfb0J6B+4E3zDAzJw1gAAAABJRU5ErkJggg==">
{{#}}}
</script>
<script>
layui.use('table', function(){
  var table = layui.table
  ,form = layui.form;
  
  table.render({
    elem: '#test'
    ,url:'./action.php?mode=getdanwelist'
    ,toolbar: '#toolbarDemo'
    ,cols: [[
      {type: 'checkbox'}
      ,{field:'id', title:'ID', width:80, sort: true}
      ,{field:'name', title:'名称', width:200}
	  ,{field:'status', title:'状态', width:120, templet: '#switchTpl'}
	  ,{field:'other', title:'外部单位', width:120, templet: '#waibu'}
    ]]
    ,page: true
  });
  form.on('switch(Changezt)', function(obj){
    //layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);
	$.post("action.php",{mode:"changedwstatus",id:this.value,zhi:obj.elem.checked},function(result){
		console.log(result);
		var r=JSON.parse(result);
		if(r.status==1){
			layer.tips('修改成功', obj.othis);
		}else{
			layer.tips('修改失败', obj.othis);
			table.reload('test', {
				url: 'action.php?mode=getdanwelist'
			});
		}
	})
  });
  //头工具栏事件
  table.on('toolbar(test)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'deleteJiTang':
        var data = checkStatus.data;
        //layer.alert(JSON.stringify(data));
		if(data.length > 0){
			var s="";
			for(var i=0;i<data.length;i++){
				s = s + data[i].id + ",";
			}
			s = s.substr(s,s.length - 1);
			//layer.alert(s);
			
			layer.confirm('确认要删除吗？删除后将无法恢复！',function(index){
				$.post("action.php",{mode:"deletedanwei",id:s},function(result){
					console.log(result);
					var r=JSON.parse(result);
					if(r.status==1){
						layer.alert('删除成功', {icon: 1});
						table.reload('test', {
							url: 'action.php?mode=getdanwelist'
						});
					}else{
						layer.alert('删除失败', {icon: 2});
					}
				})
			});
		}
      break;
    };
  });

});
</script>
</body>
</html>
<?php
}else{
	echo '<script>window.parent.location.href="login.php";</script>';
}
?>