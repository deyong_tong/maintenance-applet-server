<?php
session_start();
if(isset($_SESSION['admin'])&&$_SESSION['juese']==2){
	$id=$_GET['id'];
	include_once("config.php");
	$sql="select * from user where id=$id";
	$requ=mysqli_query($con,$sql);
	$rs=mysqli_fetch_array($requ);
	
?>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>编辑用户信息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
		<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]--></head>
    
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>姓名</label>
                        <div class="layui-input-inline">
                            <input type="text" value="<?php echo $rs['ming']; ?>" id="ming" name="ming" required="" lay-verify="required" autocomplete="off" class="layui-input"></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="phone" class="layui-form-label">
                            <span class="x-red">*</span>电话</label>
                        <div class="layui-input-inline">
                            <input type="number" value="<?php echo $rs['tel']; ?>" id="tel" name="tel" required="" lay-verify="required|phone" autocomplete="off" class="layui-input"></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>角色</label>
                        <div class="layui-input-inline">
							<select name="juese" id="juese" lay-filter="changeleibie">
								<option value="0" <?php if($rs['juese']=='0'){echo 'selected=""';} ?>>无角色</option>
								<option value="1" <?php if($rs['juese']=='1'){echo 'selected=""';} ?>>报修者</option>
								<option value="2" <?php if($rs['juese']=='2'){echo 'selected=""';} ?>>管理员</option>
								<option value="3" <?php if($rs['juese']=='3'){echo 'selected=""';} ?>>工程师</option>
							</select>
						</div>
						  <div class="layui-form-mid layui-word-aux">
							  <span class="x-red">请正确选择角色</span>
						  </div>
                    </div>
					<div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>选择单位</label>
                        <div class="layui-input-inline">
							<select name="xzdw" id="xzdw">
								<?php 
									include_once('../config.php');
									$sqll = "select id,name from danwei where status=1";
									$requu = mysqli_query($con,$sqll);
									while($rss = mysqli_fetch_array($requu)){
										if($rs['danwei'] == $rss['id']){$xz='selected=""';}else{$xz='';}
										echo '<option '.$xz.' value="'.$rss['id'].'">'.$rss['name'].'</option>';
									}
								?>
								
							</select>
						</div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red"> </span>
                      </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red"></span>新密码</label>
                        <div class="layui-input-inline">
                            <input type="password" id="xmm" name="xmm" required="" lay-verify="pass" placeholder="无需修改请留空" autocomplete="off" class="layui-input"></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red"></span>确认新密码</label>
                        <div class="layui-input-inline">
                            <input type="password" id="rxmm" name="rxmm" required="" lay-verify="repass" autocomplete="off" class="layui-input"></div>
                    </div>
                  

        <div class="layui-form-item">
            <label for="L_repass" class="layui-form-label"></label>
            <button class="layui-btn" lay-filter="add" lay-submit="">修改</button></div>
        </form>
        </div>
        </div>
        <script>layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,			
                layer = layui.layer;
				
				form.verify({
					/*
					pass: [
						/^[\S]{6,12}$/
						,'密码必须6到12位，且不能出现空格'
					],
					*/
					repass: function(value) {
						if ($('#xmm').val() != $('#rxmm').val()) {
							return '两次密码不一致';
						}
					}

				});


				form.on('submit(add)',function(data) {
					console.log(data.field);

					var d=JSON.stringify(data.field);
					$.post("action.php",{mode:"edituser",data:d,id:<?php echo $id; ?>},function(result){
						console.log(result);
						var r=JSON.parse(result);
						if(r.status!=0){
							layer.alert("修改成功", {icon: 6},function () {
								var index = parent.layer.getFrameIndex(window.name);
								parent.layer.close(index);
							});
						}else{
							layer.alert('修改失败或数据未变更。',{icon: 5})
						}
					})
					return false;
				});

            });</script>
        
    </body>

</html>
<?php
}else{
	echo '<script>window.parent.location.href="login.php";</script>';
}
?>