<?php
include_once("config.php");
include_once("PasswordStorage.php");
if(!isset($_REQUEST['mode'])){
	$result = '{"status":0}';
}else{
	$mode=$_REQUEST['mode'];
	
	if($mode=='login'){//后台登录
		$u=$_POST['u'];
		$p=$_POST['p'];
		//$p=PasswordStorage::create_hash($p);
		$sql="select password,juese,ming,id from user where username='$u' or tel = '$u'";
		$requ=mysqli_query($con,$sql);
		if($requ){
			$rs=mysqli_fetch_array($requ);
			if($rs){
				$r=PasswordStorage::verify_password($p, $rs['password']);
				if($r){
					$result = '{"status":1}';
					session_start();
					$_SESSION['admin']=$rs['id'];
					$_SESSION['juese']=$rs['juese'];
					$_SESSION['ming']=$rs['ming'];
				}else{
					$result = '{"status":0}';
				}
			}else{
				$result = '{"status":0}';
			}
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode=='logout'){//后台退出
		session_start();
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time()-42000, '/');
		}
		session_unset();
		session_destroy();
		header("location:login.php");
		die();
	}
	if($mode=='changepassword'){//修改密码
		$u=$_POST['u'];
		$o=$_POST['o'];
		$p=$_POST['p'];
		$sql="select password,id from user where username='$u' or tel = '$u'";
		$requ=mysqli_query($con,$sql);
		$rs=mysqli_fetch_array($requ);
		$r=PasswordStorage::verify_password($o, $rs['password']);
		if($r){
			$id=$rs['id'];
			$p=PasswordStorage::create_hash($p);
			$sql="update user set password='$p' where id=$id";
			mysqli_query($con,$sql);
			if(mysqli_affected_rows($con)){
				$result = '{"status":1}';
			}else{
				$result = '{"status":0}';
			}
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode=='changefenlei'){//修改故障分类信息
		$sql = "TRUNCATE `fenlei`";
		mysqli_query($con,$sql);
		$data = $_POST['data'];
		$data = json_decode($data);
		$sql = "INSERT INTO `fenlei` (`id`, `name`, `shang`, `other`) VALUES";
		foreach($data as $v){
			$id = $v->id;
			$shang = $v->pId;
			$name = $v->name;
			$sql .= "($id, '$name', $shang, ''),";
		}
		$sql = rtrim($sql,',');
		$sql.=';';
		mysqli_query($con,$sql);
		$cid = mysqli_insert_id($con);
		if($cid){
			$result='{"status":"'.$cid.'","msg":"ok"}';
		}else{
			$result='{"status":"0","msg":"请重试"}';
		}
	}
	if($mode == 'getdanwelist'){//获取单位列表
		$p=$_GET['page'];
		$l=$_GET['limit'];
		$p=($p-1)*$l;
		$sqls="select id,name,status,other from danwei where 1=1 order by other";
		$sql=$sqls." limit $p,$l";
		$requ=mysqli_query($con,$sqls);
		$num=mysqli_num_rows($requ);
		$requ=mysqli_query($con,$sql);
		$result='{"code": 0,"msg": "","count": '.$num.',"data": [';
		while($rs=mysqli_fetch_array($requ)){
			$result.='{"id":'.$rs['id'].',
						"status":"'.$rs['status'].'",
						"other":"'.$rs['other'].'",
						"name":"'.$rs['name'].'"},';
		}
		$result=rtrim($result,',');
		$result.=']}';
	}
	if($mode == 'getrenyaunlist'){//获取人员列表
		$p=$_GET['page'];
		$l=$_GET['limit'];
		$p=($p-1)*$l;
		$sqls="select a.id as id,a.ming as ming,a.status as zt,a.juese as js,
			   a.tel as tel,a.qianming as qm,danwei.name as dw,danwei.other as nw
			   from user as a 
			   left join danwei on a.danwei=danwei.id
			   where 1=1 ";
		$sql=$sqls." limit $p,$l";
		$requ=mysqli_query($con,$sqls);
		$num=mysqli_num_rows($requ);
		$requ=mysqli_query($con,$sql);
		$result='{"code": 0,"msg": "","count": '.$num.',"data": [';
		while($rs=mysqli_fetch_array($requ)){
			$result.='{"id":'.$rs['id'].',"ming":"'.$rs['ming'].'","zt":"'.$rs['zt'].'",
						"js":"'.$rs['js'].'","dw":"'.$rs['dw'].'",
						"tel":"'.$rs['tel'].'","nw":"'.$rs['nw'].'",
						"qm":"'.$rs['qm'].'"},';
		}
		$result=rtrim($result,',');
		$result.=']}';
	}	
	if($mode == 'getguanliyuanwentilist'){//管理员获取问题列表
		$zt = array("新单待审","新单驳回","管理员接单","待接单","拒接单","待维修","修完","费用驳回","维修完成","撤单","驳回后撤单","改单待审","管理员修完","费用待审","关单","","","","","",
					"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
					"","","","","","","","","","","","","","","","","","","","","","","","","","","","待填维修详情");
		if(isset($_POST['columns'])){
			$col = urldecode($_POST['columns']);
			header('content-type:application/json,charset=UTF-8');
			$sql = "select name from fenlei where shang=0";
			$requ = mysqli_query($con,$sql);
			$gz = '[';
			while($rs = mysqli_fetch_array($requ)){
				$gz.='"'.$rs['name'].'",';
			}
			$gz=rtrim($gz,',');
			$gz.=']';
			$zhuangtai = '["新单待审","新单驳回","管理员接单","待接单","拒接单","待维修","修完","费用驳回","维修完成","撤单","驳回后撤单","改单待审","管理员修完","费用待审","关单","待填维修详情"]';
			$sql = "select name from danwei where status=1 and other='0'";
			$requ = mysqli_query($con,$sql);
			$dw = '[';
			while($rs = mysqli_fetch_array($requ)){
				$dw.='"'.$rs['name'].'",';
			}
			$dw=rtrim($dw,',');
			$dw.=']';
			
			$result = '{"xm":'.$gz.',"status":'.$zhuangtai.',"bm":'.$dw.'}';
			//die(json_encode($result));
			die($result);
		}else{
			if(isset($_REQUEST['filterSos'])){
				$shaixuan = proSearchParam(json_decode(urldecode($_REQUEST['filterSos']), true), "", true);
				//$shaixuan = str_replace('A.jibie','A.huiyuan',$shaixuan);
				for($i=0;$i<count($zt);$i++){
					//状态文字替换为状态值
					$shaixuan = str_replace($zt[$i],$i,$shaixuan);
				}
				if(!empty($shaixuan)){
					$shaixuan = "And $shaixuan";
				}
			}else{
				$shaixuan = '';
			}
			//die($shaixuan);
	
	
			$p=$_GET['page'];
			$l=$_GET['limit'];
			$p=($p-1)*$l;
			$bxsj=$_REQUEST['bxsj'];
			if(empty($bxsj)){
				$bxsj='';
			}else{
				$arr = explode(" - ", $bxsj);
				$qi = strtotime($arr[0].' 0:00:00');
				$zhi = strtotime($arr[1].' 23:59:59');
				$bxsj = "and A.tbsj between $qi and $zhi";
			}

			$sqls = "select A.id as id,A.bm as bm,A.dz as dz,A.lxr as lxr,A.tel as tel,A.xm as xm,
					A.yysj as yysj,A.tbsj as tbsj,A.status as status,A.clsj as clsj,
					A.gz as gz,A.ms as ms,A.img as img,A.xxzxyj as xxzxyj,A.bxzqm as bxzqm,
					A.wxxm as wxxm,A.zj as zj,A.wxsj as wxsj,A.wxzqm as wxzqm,
					A.xxzxqm as xxzxqm,A.shsj as shsj,A.shyj as shyj,
					u4.ming as xxzxshz,
					A.pdsj as pdsj,A.clyj as clyj,u3.tel as gcsdh,u1.tel as tbrdh,
					u1.ming as bxr,u2.ming as xxzx,u3.ming as wxz 
					from wenti as A 
					left join user as u1 on A.uid=u1.openid 
					left join user as u2 on A.xxzx=u2.openid 
					left join user as u3 on A.wxz=u3.openid 
					left join user as u4 on A.xxzxshz=u4.openid
					where 1=1 $bxsj $shaixuan
					order by A.id desc";
			$sql=$sqls." limit $p,$l";
			$requ=mysqli_query($con,$sqls);
			if (!$requ) {
				echo $sql.'<br><br>';
				printf("Error: %s\n", mysqli_error($con));
				exit();
			}
			$num=mysqli_num_rows($requ);
			$requ=mysqli_query($con,$sql);
			$result='{"code": 0,"msg": "","count": '.$num.',"data": [';
			while($rs=mysqli_fetch_array($requ)){
				//ID，部门，地址，联系人，电话，维修项目，故障，描述，预约时间，报修时间，
				//状态代码，图片，报修人，信息中心审核人，工程师，拒单原因，派单时间，信息中心意见，
				//工程师电话，报修人电话，拒单时间， 总价， 维修详情， 维修时间，工程师签名， 被修者签名
				$result.='{"id":"'.$rs['id'].'","bm":"'.$rs['bm'].'","dz":"'.$rs['dz'].'",
						"lxr":"'.$rs['lxr'].'","tel":"'.$rs['tel'].'","xm":"'.$rs['xm'].'",
						"gz":"'.$rs['gz'].'","ms":"'.str_replace(PHP_EOL, '', $rs['ms']).'","yysj":"'.$rs['yysj'].'",
						"tbsj":"'.date("Y-m-d H:i:s",$rs['tbsj']).'","status":"'.$zt[$rs['status']].'",
						"img":"'.str_replace('"',"'",$rs['img']).'","bxr":"'.$rs['bxr'].'",
						"xxzx":"'.$rs['xxzx'].'","wxz":"'.$rs['wxz'].'","clyj":"'.$rs['clyj'].'",
						"pdsj":"'.date("Y-m-d H:i",$rs['pdsj']).'","xxzxyj":"'.$rs['xxzxyj'].'",
						"gcsdh":"'.$rs['gcsdh'].'","bxrdh":"'.$rs['tbrdh'].'",
						"jdsj":"'.date("Y-m-d H:i:s",$rs['clsj']).'","zj":"'.$rs['zj'].'",
						"wxxm":"'.str_replace('"',"'",$rs['wxxm']).'","wxsj":"'.date("Y-m-d H:i",$rs['wxsj']).'",
						"gcsqm":"'.$rs['wxzqm'].'","bxzqm":"'.$rs['bxzqm'].'","xxzxqm":"'.$rs['xxzxqm'].'",
						"shsj":"'.date("Y-m-d H:i:s",$rs['shsj']).'","shyj":"'.$rs['shyj'].'",
						"xxzxshz":"'.$rs['xxzxshz'].'"
						},';
			}
			$result = rtrim($result,',');
			$result.=']}';
			$result=str_replace('1970-01-01 08:00:00','-',$result);
			$result=str_replace('1970-01-01 08:00','-',$result);
		}
	}
	if($mode == 'changedwstatus'){//修改单位状态
		$id=$_REQUEST['id'];
		$v=$_REQUEST['zhi'];
		$sql = "update danwei set status=$v where id=$id";
		mysqli_query($con,$sql);
		if(mysqli_affected_rows($con)){
			$result = '{"status":1}';
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode == 'changerystatus'){//修改人员状态
		$id=$_REQUEST['id'];
		$v=$_REQUEST['zhi'];
		$sql = "update user set status=$v where id=$id";
		mysqli_query($con,$sql);
		if(mysqli_affected_rows($con)){
			$result = '{"status":1}';
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode == 'deletedanwei'){//删除单位
		$id = $_POST['id'];
		$sql = "delete from danwei where id in ($id)";
		mysqli_query($con,$sql);
		if(mysqli_affected_rows($con)){
			$result = '{"status":1}';
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode == 'deleterenyuan'){//删除人员
		$id = $_POST['id'];
		$sql = "delete from user where id in ($id)";
		mysqli_query($con,$sql);
		if(mysqli_affected_rows($con)){
			$result = '{"status":1}';
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode == 'adddanwei'){//添加单位
		$dwm=$_POST['dwm'];
		$wdw=$_POST['wdw'];
		$sql = "insert into danwei (name, status, other) values ('$dwm', 1, '$wdw')";
		mysqli_query($con,$sql);
		if(mysqli_insert_id($con)){
			$result='{"status":"1","msg":"ok"}';
		}else{
			$result='{"status":"0","msg":"请重试"}';
		}
	}
	if($mode == 'addrenyuan'){//添加人员
		$dwm=$_POST['dwm'];
		$tel=$_POST['tel'];
		$xzdw=$_POST['xzdw'];
		$xzjs=$_POST['xzjs'];
		$sql = "insert into user (openid,username,password,juese,status,ming,tel,danwei,qianming) values 
								 ('','$tel','',$xzjs,1,'$dwm','$tel',$xzdw,'')";
		mysqli_query($con,$sql);
		if(mysqli_insert_id($con)){
			$result='{"status":"1","msg":"ok"}';
		}else{
			$result='{"status":"0","msg":"请重试"}';
		}
	}
	if($mode == 'edituser'){//编辑用户信息
		$id=$_POST['id'];
		$data=$_POST['data'];
		$data=json_decode($data);
		$ming=$data->ming;
		$tel=$data->tel;
		$juese=$data->juese;
		$dw=$data->xzdw;
		$mm=$data->xmm;
		if(!empty($mm)){
			$p=md5($mm);
			$p=PasswordStorage::create_hash($p);
			$mm=",password='$p'";
		}else{
			$mm="";
		}
		$sql = "update user set ming='$ming',tel='$tel',danwei=$dw,juese=$juese$mm where id=$id";
		mysqli_query($con,$sql);
		if(mysqli_affected_rows($con)){
			$result = '{"status":1}';
		}else{
			$result = '{"status":0}';
		}
	}
	if($mode == 'getgcswentilist'){//工程师获取问题列表
		$zt = array("新单待审","新单驳回","管理员接单","待接单","拒接单","待维修","修完","费用驳回","维修完成","撤单","驳回后撤单","改单待审","管理员修完","费用待审","关单","","","","","",
					"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
					"","","","","","","","","","","","","","","","","","","","","","","","","","","","待填维修详情");
		if(isset($_POST['columns'])){
			$col = urldecode($_POST['columns']);
			header('content-type:application/json,charset=UTF-8');
			$sql = "select name from fenlei where shang=0";
			$requ = mysqli_query($con,$sql);
			$gz = '[';
			while($rs = mysqli_fetch_array($requ)){
				$gz.='"'.$rs['name'].'",';
			}
			$gz=rtrim($gz,',');
			$gz.=']';
			$zhuangtai = '["新单待审","新单驳回","管理员接单","待接单","拒接单","待维修","修完","费用驳回","维修完成","撤单","驳回后撤单","改单待审","管理员修完","费用待审","关单","待填维修详情"]';
			$sql = "select name from danwei where status=1 and other='0'";
			$requ = mysqli_query($con,$sql);
			$dw = '[';
			while($rs = mysqli_fetch_array($requ)){
				$dw.='"'.$rs['name'].'",';
			}
			$dw=rtrim($dw,',');
			$dw.=']';
			
			$result = '{"xm":'.$gz.',"status":'.$zhuangtai.',"bm":'.$dw.'}';
			//die(json_encode($result));
			die($result);
		}else{
			if(isset($_REQUEST['filterSos'])){
				$shaixuan = proSearchParam(json_decode(urldecode($_REQUEST['filterSos']), true), "", true);
				//$shaixuan = str_replace('A.jibie','A.huiyuan',$shaixuan);
				for($i=0;$i<count($zt);$i++){
					//状态文字替换为状态值
					$shaixuan = str_replace($zt[$i],$i,$shaixuan);
				}
				if(!empty($shaixuan)){
					$shaixuan = "And $shaixuan";
				}
			}else{
				$shaixuan = '';
			}
			//die($shaixuan);
	
			$gcs=$_REQUEST['gcs'];
			$sql="select openid from user where danwei in (select danwei from user where id=$gcs)";
			$requ = mysqli_query($con,$sql);
			$us='';
			while($rs = mysqli_fetch_array($requ)){
				$us.="'".$rs['openid']."',";
			}
			$us = rtrim($us,',');
			$us = " and A.wxz in ($us)";
			
			
			$p=$_GET['page'];
			$l=$_GET['limit'];
			$p=($p-1)*$l;
			$bxsj=$_REQUEST['bxsj'];
			if(empty($bxsj)){
				$bxsj='';
			}else{
				$arr = explode(" - ", $bxsj);
				$qi = strtotime($arr[0].' 0:00:00');
				$zhi = strtotime($arr[1].' 23:59:59');
				$bxsj = "and A.wxsj between $qi and $zhi";
			}

			$sqls = "select A.id as id,A.bm as bm,A.dz as dz,A.lxr as lxr,A.tel as tel,A.xm as xm,
					A.yysj as yysj,A.tbsj as tbsj,A.status as status,A.clsj as clsj,
					A.gz as gz,A.ms as ms,A.img as img,A.xxzxyj as xxzxyj,A.bxzqm as bxzqm,
					A.wxxm as wxxm,A.zj as zj,A.wxsj as wxsj,A.wxzqm as wxzqm,
					A.xxzxqm as xxzxqm,A.shsj as shsj,A.shyj as shyj,
					u4.ming as xxzxshz,A.daochu as daochu,
					A.pdsj as pdsj,A.clyj as clyj,u3.tel as gcsdh,u1.tel as tbrdh,
					u1.ming as bxr,u2.ming as xxzx,u3.ming as wxz 
					from wenti as A 
					left join user as u1 on A.uid=u1.openid 
					left join user as u2 on A.xxzx=u2.openid 
					left join user as u3 on A.wxz=u3.openid 
					left join user as u4 on A.xxzxshz=u4.openid
					where 1=1 $bxsj $us $shaixuan
					order by A.id desc";
			$sql=$sqls." limit $p,$l";
			$requ=mysqli_query($con,$sqls);
			if (!$requ) {
				echo $sql.'<br><br>';
				printf("Error: %s\n", mysqli_error($con));
				exit();
			}
			$num=mysqli_num_rows($requ);
			$requ=mysqli_query($con,$sql);
			$result='{"code": 0,"msg": "","count": '.$num.',"data": [';
			while($rs=mysqli_fetch_array($requ)){
				//ID，部门，地址，联系人，电话，维修项目，故障，描述，预约时间，报修时间，
				//状态代码，图片，报修人，信息中心审核人，工程师，拒单原因，派单时间，信息中心意见，
				//工程师电话，报修人电话，拒单时间， 总价， 维修详情， 维修时间，工程师签名， 被修者签名
				$result.='{"id":"'.$rs['id'].'","bm":"'.$rs['bm'].'","dz":"'.$rs['dz'].'",
						"lxr":"'.$rs['lxr'].'","tel":"'.$rs['tel'].'","xm":"'.$rs['xm'].'",
						"gz":"'.$rs['gz'].'","ms":"'.str_replace(PHP_EOL, '', $rs['ms']).'","yysj":"'.$rs['yysj'].'",
						"tbsj":"'.date("Y-m-d H:i:s",$rs['tbsj']).'","status":"'.$zt[$rs['status']].'",
						"img":"'.str_replace('"',"'",$rs['img']).'","bxr":"'.$rs['bxr'].'",
						"xxzx":"'.$rs['xxzx'].'","wxz":"'.$rs['wxz'].'","clyj":"'.$rs['clyj'].'",
						"pdsj":"'.date("Y-m-d H:i",$rs['pdsj']).'","xxzxyj":"'.$rs['xxzxyj'].'",
						"gcsdh":"'.$rs['gcsdh'].'","bxrdh":"'.$rs['tbrdh'].'",
						"jdsj":"'.date("Y-m-d H:i:s",$rs['clsj']).'","zj":"'.$rs['zj'].'",
						"wxxm":"'.str_replace('"',"'",$rs['wxxm']).'","wxsj":"'.date("Y-m-d H:i",$rs['wxsj']).'",
						"gcsqm":"'.$rs['wxzqm'].'","bxzqm":"'.$rs['bxzqm'].'","xxzxqm":"'.$rs['xxzxqm'].'",
						"shsj":"'.date("Y-m-d H:i:s",$rs['shsj']).'","shyj":"'.$rs['shyj'].'",
						"xxzxshz":"'.$rs['xxzxshz'].'","daochu":"'.$rs['daochu'].'","zhuangtai":"'.$rs['status'].'"
						},';
			}
			$result = rtrim($result,',');
			$result.=']}';
			$result=str_replace('1970-01-01 08:00:00','-',$result);
			$result=str_replace('1970-01-01 08:00','-',$result);
		}
	}
	
	
	
	echo $result;
}	
	
	/**
     * 后台筛选处理方法
     * @param $filterSos
     * @param $sql
     * @param bool $first
     * @param array $field
     * @return string
     */
    function proSearchParam($filterSos, $sql, $first = false, $field = [])
    {
        foreach ($filterSos as $item) {
            if(isset($item['values']) && empty($item['values'])){
                //设置了values并且为空则跳出
                continue;
            }

            if(isset($item['value']) && empty($item['value'])){
                //设置了value并且为空则跳出
                continue;
            }

            //改写字段所属表
            if($item['mode'] != "group") {
                $item['field'] = isset($field[$item['field']]) ? $field[$item['field']] . '.' . $item['field'] : 'A.' . $item['field'];
            }else{
                if(count($item['children']) == 0){
                    continue;
                }
            }

            if($item['prefix'] == "and"){
                if($first){
                    $sql .= " (";
                }else {
                    $sql .= " AND (";
                }
            }else{
                $sql .= " OR (";
            }

            switch ($item['mode']) {
                case "in" :
                    {
                        $str = implode('","', $item['values']);
                        $sql .= $item['field'] . ' in ("' . $str . '"))';
                        break;
                    }
                case "condition" :
                    {
                        if ($item['type'] == "contain") {
                            $sql .= $item['field'] . " like " . "'%" . $item['value'] . "%'" . ")";
                        } else if ($item['type'] == "notContain") {
                            $sql .= $item['field'] . " not like " . "'%" . $item['value'] . "%'" . ")";
                        } else if ($item['type'] == "start") {
                            $sql .= $item['field'] . " like '" . $item['value'] . "%'" . ")";
                        } else if ($item['type'] == "end") {
                            $sql .= $item['field'] . " like '" . "%" . $item['value'] . "')";
                        } else if ($item['type'] == "eq") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " = " . $item['value'] . ")";
                        } else if ($item['type'] == "ne") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " <> " . $item['value'] . ")";
                        } else if ($item['type'] == "gt") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " > " . $item['value'] . ")";
                        } else if ($item['type'] == "ge") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " >= " . $item['value'] . ")";
                        } else if ($item['type'] == "lt") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " < " . $item['value'] . ")";
                        } else if ($item['type'] == "le") {
                            $item['value'] = is_numeric($item['value']) ? $item['value'] : "'" . $item['value'] . "'";
                            $sql .= $item['field'] . " <= " . $item['value'] . ")";
                        } else {
                            $sql .= $item['field'] . $item['value'] . ")";
                        }
                        break;
                    }
                case "date" :
                    {
                        if ($item['type'] == "yesterday") {
                            $sql .= $item['field'] . " between '" . strtotime(date("Y-m-d 00:00:00", strtotime("-1 day"))) . "' AND '" . strtotime(date("Y-m-d 23:59:59")) . "')";
                        } else if ($item['type'] == "thisWeek") {
                            $sql .= $item['field'] . " between '"
                                . strtotime(date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - date("w") + 1, date("Y"))))
                                . "' AND '"
                                . strtotime(date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - date("w") + 7, date("Y"))))
                                . "')";
                        } else if ($item['type'] == "lastWeek") {
                            $sql .= $item['field'] . " between '"
                                . strtotime(date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - date("w") - 7, date("Y"))))
                                . "' AND '"
                                . strtotime(date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - date("w"), date("Y"))))
                                . "')";
                        } else if ($item['type'] == "thisMonth") {
                            $sql .= $item['field'] . " between '"
                                . strtotime(date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), 1, date("Y"))))
                                . "' AND '"
                                . strtotime(date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("t"), date("Y"))))
                                . "')";
                        } else if ($item['type'] == "thisYear") {
                            $sql .= $item['field'] . " between '"
                                . strtotime(date('Y-01-01'))
                                . "' AND '"
                                . strtotime(date('Y-12-31'))
                                . "')";
                        } else if ($item['type'] == "specific") {
                            //指定时间
                            $sql .= $item['field'] . " between '" . strtotime($item['value'] . " 00:00:00")
                                . "' AND '"
                                . strtotime($item['value'] . " 23:59:59")
                                . "')";
                        } else if ($item['type'] == "exclude") {
                            //排除时间
                            $sql .= $item['field'] . " not between '" . strtotime($item['value'] . " 00:00:00")
                                . "' AND '"
                                . strtotime($item['value'] . " 23:59:59")
                                . "')";
                        } else if ($item['type'] == "specificSlot") {
                            //指定时间段
                            $sql .= $item['field'] . " between '" . strtotime(explode(' ~ ', $item['value'])[0])
                                . "' AND '"
                                . strtotime(date("Y-m-d 23:59:59", strtotime(explode(' ~ ', $item['value'])[1])))
                                . "')";
                        } else if ($item['type'] == "excludeSlot") {
                            //排除时间段
                            $sql .= $item['field'] . " not between '" . strtotime(explode(' ~ ', $item['value'])[0])
                                . "' AND '"
                                . strtotime(date("Y-m-d 23:59:59", strtotime(explode(' ~ ', $item['value'])[1])))
                                . "')";
                        } else if ($item['type'] == "all") {
                            $sql .= "1" . "=" . 1 . ")";
                        }
                        break;
                    }
                case "group" :
                    {
                        $sql = proSearchParam($item['children'], $sql, true, $field) . ")";
                        break;
                    }
            }
            $first = false;
        }
        return $sql;
    }