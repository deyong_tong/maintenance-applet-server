<?php
include_once("../config.php");

$sql = "select a.id as id,a.bm as bm,a.dz as dz,a.lxr as lxr,a.tel as tel,a.xm as xm,
		a.yysj as yysj,a.tbsj as tbsj,a.status as status,
		a.gz as gz,a.ms as ms,a.img as img,user.ming as bxr 
		from wenti as a 
		left join user on a.uid=user.openid 
		where a.status in (0,11)";
$requ = mysqli_query($con,$sql);
$dsh='[';//报修者提报，待管理员审批的订单
while($rs=mysqli_fetch_array($requ)){
	//ID，部门，地址，联系人，电话，维修项目，故障，描述，预约时间，报修时间，状态代码，图片，报修人
	$dsh.='{"id":"'.$rs['id'].'","bm":"'.$rs['bm'].'","dz":"'.$rs['dz'].'",
			"lxr":"'.$rs['lxr'].'","tel":"'.$rs['tel'].'","xm":"'.$rs['xm'].'",
			"gz":"'.$rs['gz'].'","ms":"'.str_replace(PHP_EOL, '', $rs['ms']).'","yysj":"'.$rs['yysj'].'",
			"tbsj":"'.date("Y-m-d H:i:s",$rs['tbsj']).'","status":"'.$rs['status'].'",
			"img":"'.str_replace('"',"'",$rs['img']).'","bxr":"'.$rs['bxr'].'"},';
}
$dsh=rtrim($dsh,",");
$dsh.=']';



$sql = "select a.id as id,a.bm as bm,a.dz as dz,a.lxr as lxr,a.tel as tel,a.xm as xm,
		a.yysj as yysj,a.tbsj as tbsj,a.status as status,a.clsj as clsj,
		a.gz as gz,a.ms as ms,a.img as img,a.xxzxyj as xxzxyj,
		a.pdsj as pdsj,a.clyj as clyj,u3.tel as gcsdh,u1.tel as tbrdh,
		u1.ming as bxr,u2.ming as xxzx,u3.ming as wxz 
		from wenti as a 
		left join user as u1 on a.uid=u1.openid 
		left join user as u2 on a.xxzx=u2.openid 
		left join user as u3 on a.wxz=u3.openid 
		where a.status in (4)";
$requ = mysqli_query($con,$sql);
$dpd='[';//工程师退回的单，需要重新处理
while($rs=mysqli_fetch_array($requ)){
	//ID，部门，地址，联系人，电话，维修项目，故障，描述，预约时间，报修时间，
	//状态代码，图片，报修人，信息中心审核人，工程师，拒单原因，派单时间，信息中心意见，
	//工程师电话，报修人电话，拒单时间
	$dpd.='{"id":"'.$rs['id'].'","bm":"'.$rs['bm'].'","dz":"'.$rs['dz'].'",
			"lxr":"'.$rs['lxr'].'","tel":"'.$rs['tel'].'","xm":"'.$rs['xm'].'",
			"gz":"'.$rs['gz'].'","ms":"'.str_replace(PHP_EOL, '', $rs['ms']).'","yysj":"'.$rs['yysj'].'",
			"tbsj":"'.date("Y-m-d H:i:s",$rs['tbsj']).'","status":"'.$rs['status'].'",
			"img":"'.str_replace('"',"'",$rs['img']).'","bxr":"'.$rs['bxr'].'",
			"xxzx":"'.$rs['xxzx'].'","wxz":"'.$rs['wxz'].'","clyj":"'.$rs['clyj'].'",
			"pdsj":"'.date("Y-m-d H:i",$rs['pdsj']).'","xxzxyj":"'.$rs['xxzxyj'].'",
			"gcsdh":"'.$rs['gcsdh'].'","bxrdh":"'.$rs['tbrdh'].'",
			"jdsj":"'.date("Y-m-d H:i:s",$rs['clsj']).'"},';
}
$dpd=rtrim($dpd,",");
$dpd.=']';



$sql = "select a.id as id,a.bm as bm,a.dz as dz,a.lxr as lxr,a.tel as tel,a.xm as xm,
		a.yysj as yysj,a.tbsj as tbsj,a.status as status,a.clsj as clsj,
		a.gz as gz,a.ms as ms,a.img as img,a.xxzxyj as xxzxyj,a.bxzqm as bxzqm,
		a.wxxm as wxxm,a.zj as zj,a.wxsj as wxsj,a.wxzqm as wxzqm,
		a.pdsj as pdsj,a.clyj as clyj,u3.tel as gcsdh,u1.tel as tbrdh,
		u1.ming as bxr,u2.ming as xxzx,u3.ming as wxz 
		from wenti as a 
		left join user as u1 on a.uid=u1.openid 
		left join user as u2 on a.xxzx=u2.openid 
		left join user as u3 on a.wxz=u3.openid 
		where a.status in (6)";
$requ = mysqli_query($con,$sql);
$dfk='[';//工程师维修完成的单，需要审核费用
while($rs=mysqli_fetch_array($requ)){
	//ID，部门，地址，联系人，电话，维修项目，故障，描述，预约时间，报修时间，
	//状态代码，图片，报修人，信息中心审核人，工程师，拒单原因，派单时间，信息中心意见，
	//工程师电话，报修人电话，拒单时间， 总价， 维修详情， 维修时间，工程师签名， 被修者签名
	$dfk.='{"id":"'.$rs['id'].'","bm":"'.$rs['bm'].'","dz":"'.$rs['dz'].'",
			"lxr":"'.$rs['lxr'].'","tel":"'.$rs['tel'].'","xm":"'.$rs['xm'].'",
			"gz":"'.$rs['gz'].'","ms":"'.str_replace(PHP_EOL, '', $rs['ms']).'","yysj":"'.$rs['yysj'].'",
			"tbsj":"'.date("Y-m-d H:i:s",$rs['tbsj']).'","status":"'.$rs['status'].'",
			"img":"'.str_replace('"',"'",$rs['img']).'","bxr":"'.$rs['bxr'].'",
			"xxzx":"'.$rs['xxzx'].'","wxz":"'.$rs['wxz'].'","clyj":"'.$rs['clyj'].'",
			"pdsj":"'.date("Y-m-d H:i",$rs['pdsj']).'","xxzxyj":"'.$rs['xxzxyj'].'",
			"gcsdh":"'.$rs['gcsdh'].'","bxrdh":"'.$rs['tbrdh'].'",
			"jdsj":"'.date("Y-m-d H:i:s",$rs['clsj']).'","zj":"'.$rs['zj'].'",
			"wxxm":"'.str_replace('"',"'",$rs['wxxm']).'","wxsj":"'.date("Y-m-d H:i",$rs['wxsj']).'",
			"gcsqm":"'.$rs['wxzqm'].'","bxzqm":"'.$rs['bxzqm'].'"
			},';
}
$dfk=rtrim($dfk,",");
$dfk.=']';



$result = '{"status":"1","msg":"ok","dfk":'.$dfk.',"dpd":'.$dpd.',"dsh":'.$dsh.'}';

echo $result;
















